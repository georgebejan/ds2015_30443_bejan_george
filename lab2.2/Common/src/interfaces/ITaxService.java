package interfaces;

import models.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author George Bejan
 */
public interface ITaxService extends Remote {
    double computeTax(Car c) throws RemoteException;

    double sellingPrice(Car c) throws RemoteException;
}
