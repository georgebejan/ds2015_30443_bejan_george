package utils;

/**
 * @author George Bejan
 */
public class Constants {
    public static final String RMI_ID = "TestRMI";
    public static final int RMI_PORT = 222;
}
