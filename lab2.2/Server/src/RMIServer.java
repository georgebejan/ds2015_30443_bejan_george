import services.TaxService;
import utils.Constants;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author George Bejan
 */
public class RMIServer {
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        TaxService taxService = new TaxService();
        final Registry registry = LocateRegistry.createRegistry(Constants.RMI_PORT);
        registry.bind(Constants.RMI_ID, taxService);
    }
}
