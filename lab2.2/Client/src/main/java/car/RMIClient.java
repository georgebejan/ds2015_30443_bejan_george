package car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * @author George Bejan
 */
@SpringBootApplication
public class RMIClient {
    public static void main(String[] args) throws RemoteException, NotBoundException {
        SpringApplication.run(RMIClient.class, args);
    }
}
