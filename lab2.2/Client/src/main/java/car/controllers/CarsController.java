package car.controllers;

import car.dto.CarDTO;
import car.dto.ResultDTO;
import interfaces.ITaxService;
import models.Car;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import utils.Constants;

import javax.validation.Valid;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author George Bejan
 */
@Controller
public class CarsController extends WebMvcConfigurerAdapter {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showForm(Model model) {
        model.addAttribute("car", new CarDTO());
        model.addAttribute("result", new ResultDTO());
        return "car";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String computeTax(@Valid CarDTO car, BindingResult bindingResult, Model model) throws RemoteException, NotBoundException {
        model.addAttribute("car", car);
        if (bindingResult.hasErrors()) {
            return "car";
        } else {
            final Registry registry = LocateRegistry.getRegistry("localhost", Constants.RMI_PORT);
            final ITaxService taxService = (ITaxService) registry.lookup(Constants.RMI_ID);
            final Car c = new Car(car.getYear(), car.getEngineCapacity(), car.getPrice());
            final double tax = taxService.computeTax(c);
            final double sellingPrice = taxService.sellingPrice(c);
            model.addAttribute("result", new ResultDTO(tax, sellingPrice));
            return "car";
        }
    }
}
