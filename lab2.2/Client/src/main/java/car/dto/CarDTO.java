package car.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author George Bejan
 */
public class CarDTO {
    @NotNull
    @Min(1)
    private int year;
    @NotNull
    @Min(1)
    private int engineCapacity;
    @NotNull
    @Min(1)
    private double price;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
