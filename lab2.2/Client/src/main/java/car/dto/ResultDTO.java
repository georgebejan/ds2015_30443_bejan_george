package car.dto;

/**
 * @author George Bejan
 */
public class ResultDTO {
    private double tax;
    private double sellingPrice;

    public ResultDTO() {

    }

    public ResultDTO(double tax, double sellingPrice) {
        this.tax = tax;
        this.sellingPrice = sellingPrice;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
}
