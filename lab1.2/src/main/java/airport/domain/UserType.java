package airport.domain;

/**
 * @author George Bejan, Catalysts GmbH
 */
public enum UserType {
    CLIENT, ADMIN
}
