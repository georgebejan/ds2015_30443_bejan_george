package airport.dao;

import airport.domain.Flight;

/**
 * @author George Bejan
 */
public class FlightDAO extends AbstractDAO<Flight> {
    @Override
    protected Class<Flight> getEntityClass() {
        return Flight.class;
    }
}
