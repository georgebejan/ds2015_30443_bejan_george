package airport.dao;

import airport.domain.City;

/**
 * @author George Bejan, Catalysts GmbH
 */
public class CityDAO extends AbstractDAO<City> {
    @Override
    protected Class<City> getEntityClass() {
        return City.class;
    }
}
