package airport.dao;

import airport.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;


/**
 * @author George Bejan, Catalysts GmbH
 */
public abstract class AbstractDAO<ENTITY> {
    protected Session session;
    protected Transaction transaction;

    protected abstract Class<ENTITY> getEntityClass();

    public Transaction getTransaction() {
        return transaction;
    }

    public Session getSession() {
        return session;
    }

    protected void openSession() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    protected void closeSession() {
        session.close();
    }

    protected void openTransaction() {
        openSession();
        transaction = getSession().beginTransaction();
    }

    protected void closeTransaction() {
        getTransaction().commit();
        closeSession();
    }

    public void save(final ENTITY entity) {
        openTransaction();
        session.saveOrUpdate(entity);
        closeTransaction();
    }

    @SuppressWarnings("unchecked")
    public ENTITY findById(final int id) {
        openTransaction();
        final ENTITY entity = (ENTITY) getSession().get(getEntityClass(), id);
        closeTransaction();
        return entity;
    }

    public void delete(final ENTITY entity) {
        openTransaction();
        session.delete(entity);
        closeTransaction();
    }

    @SuppressWarnings("unchecked")
    public List<ENTITY> findAll() {
        openTransaction();
        final List<ENTITY> entities = (List<ENTITY>) getSession().createQuery("from " + getEntityClass().getName()).list();
        closeTransaction();
        return entities;
    }
}
