package airport.dao;

import airport.domain.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 * @author George Bejan, Catalysts GmbH
 */
public class UserDAO extends AbstractDAO<User> {
    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    public User findByUsernameAndPassword(final String username, final String password) {
        openTransaction();
        final Criteria criteria = getSession().createCriteria(User.class);
        final User user = (User) criteria.add(Restrictions.and(Restrictions.and(
                Restrictions.eq("username", username),
                Restrictions.eq("password", password)))).uniqueResult();
        closeTransaction();
        return user;
    }
}
