package airport.service;

import airport.dao.AbstractDAO;
import airport.dao.FlightDAO;
import airport.domain.Flight;
import airport.dto.FlightDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author George Bejan
 */
public class FlightService extends AbstractCRUDLService<Flight> {
    @Override
    protected AbstractDAO<Flight> getDao() {
        return new FlightDAO();
    }


    public void save(FlightDTO dto) {
        final Flight flight = new Flight();
        flight.setId(dto.getId());
        flight.setNumber(dto.getNumber());
        flight.setAirplaneType(dto.getAirplaneType());
        flight.setDepartureCity(dto.getDepartureCity());
        flight.setArrivalCity(dto.getArrivalCity());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        final String arrivalDate = dto.getArrivalDate() + " " + dto.getArrivalTime();
        try {
            flight.setArrivalDate(sdf.parse(arrivalDate));
        } catch (ParseException e) {
            flight.setArrivalDate(null);
        }

        final String departureDate = dto.getDepartureDate() + " " + dto.getDepartureTime();
        try {
            flight.setDepartureDate(sdf.parse(departureDate));
        } catch (ParseException e) {
            flight.setDepartureDate(null);
        }
        save(flight);
    }
}
