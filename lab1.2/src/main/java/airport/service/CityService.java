package airport.service;

import airport.dao.AbstractDAO;
import airport.dao.CityDAO;
import airport.domain.City;

/**
 * @author George Bejan, Catalysts GmbH
 */
public class CityService extends AbstractCRUDLService<City> {
    @Override
    protected AbstractDAO<City> getDao() {
        return new CityDAO();
    }
}
