package airport.service;

import airport.dao.AbstractDAO;
import airport.dao.UserDAO;
import airport.domain.User;
import airport.dto.LoginDTO;

/**
 * @author George Bejan, Catalysts GmbH
 */
public class UserService extends AbstractCRUDLService<User> {
    private UserDAO userDAO;

    public UserService() {
        userDAO = new UserDAO();
    }

    @Override
    protected AbstractDAO<User> getDao() {
        return userDAO;
    }

    public User login(final LoginDTO dto) {
        return userDAO.findByUsernameAndPassword(dto.getName(), dto.getPassword());
    }
}
