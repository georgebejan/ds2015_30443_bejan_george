package airport.service;

import airport.dao.AbstractDAO;

import java.util.List;

/**
 * @author George Bejan, Catalysts GmbH
 */
public abstract class AbstractCRUDLService<ENTITY> {
    public void save(final ENTITY entity) {
        getDao().save(entity);
    }

    public ENTITY findById(final int id) {
        return getDao().findById(id);
    }

    public void delete(final ENTITY entity) {
        getDao().delete(entity);
    }

    public List<ENTITY> findAll() {
        return getDao().findAll();
    }

    protected abstract AbstractDAO<ENTITY> getDao();
}
