package airport.controller;

import airport.domain.City;
import airport.service.CityService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author George Bejan
 */
@WebServlet("/cities")
public class CityController extends HttpServlet {
    private CityService cityService;

    public CityController() {
        cityService = new CityService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final boolean isAdmin = (Boolean) request.getSession().getAttribute("isAdmin");
        if (isAdmin) {
            final List<City> cities = cityService.findAll();
            final ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(new Gson().toJson(cities).getBytes());
            outputStream.flush();
            outputStream.close();
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
