package airport.controller;

import airport.domain.User;
import airport.domain.UserType;
import airport.dto.LoginDTO;
import airport.service.UserService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author George Bejan
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
    private UserService userService;

    public LoginController() {
        userService = new UserService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final HttpSession session = request.getSession();
        final String admin = "isAdmin";

        final LoginDTO dto = new Gson().fromJson(request.getReader(), LoginDTO.class);
        final User user = userService.login(dto);
        if (user != null) {
            if (user.getType().equals(UserType.ADMIN)) {
                session.setAttribute(admin, true);
            } else {
                session.setAttribute(admin, false);
            }
            response.setStatus(HttpServletResponse.SC_OK);
            final ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(new Gson().toJson(user).getBytes());
            outputStream.flush();
            outputStream.close();
        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final Object cookie = request.getSession().getAttribute("isAdmin");
        if (cookie == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        final boolean isAdmin = (Boolean) cookie;
        final ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(new Gson().toJson(isAdmin).getBytes());
        outputStream.flush();
        outputStream.close();
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
    }
}