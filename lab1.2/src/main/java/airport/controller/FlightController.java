package airport.controller;

import airport.domain.Flight;
import airport.dto.FlightDTO;
import airport.service.FlightService;
import airport.util.FlightConverter;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author George Bejan, Catalysts GmbH
 */
@WebServlet("/flights")
public class FlightController extends HttpServlet {
    private FlightService flightService;

    public FlightController() {
        flightService = new FlightService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final ServletOutputStream outputStream = response.getOutputStream();
        final Object cookie = request.getSession().getAttribute("isAdmin");
        if (cookie == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        final boolean isAdmin = (Boolean) cookie;
        final String id = request.getParameter("id");
        if (id != null) {
            if (isAdmin) {
                final Flight flight = flightService.findById(Integer.parseInt(id));
                FlightDTO dto = FlightConverter.toDTO(flight);
                outputStream.write(new Gson().toJson(dto).getBytes());
                outputStream.flush();
                outputStream.close();
                return;
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        final List<Flight> flights = flightService.findAll();
        outputStream.write(new Gson().toJson(flights).getBytes());
        outputStream.flush();
        outputStream.close();
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final Object cookie = request.getSession().getAttribute("isAdmin");
        if (cookie == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        final boolean isAdmin = (Boolean) cookie;
        if (isAdmin) {
            final FlightDTO flight = new Gson().fromJson(request.getReader(), FlightDTO.class);
            flightService.save(flight);
        } else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final Object cookie = request.getSession().getAttribute("isAdmin");
        if (cookie == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        final boolean isAdmin = (Boolean) cookie;
        if (isAdmin) {
            final String id = request.getParameter("id");
            if (id != null) {
                final Flight flight = flightService.findById(Integer.parseInt(id));
                flightService.delete(flight);
            }
        }
    }
}
