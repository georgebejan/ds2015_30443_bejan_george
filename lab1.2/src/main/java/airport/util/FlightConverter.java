package airport.util;

import airport.domain.Flight;
import airport.dto.FlightDTO;

import java.util.Calendar;

/**
 * @author George Bejan, Catalysts GmbH
 */
public class FlightConverter {
    public static FlightDTO toDTO(final Flight flight) {
        final FlightDTO dto = new FlightDTO();
        dto.setId(flight.getId());
        dto.setNumber(flight.getNumber());
        dto.setAirplaneType(flight.getAirplaneType());
        dto.setDepartureCity(flight.getDepartureCity());
        dto.setArrivalCity(flight.getArrivalCity());

        Calendar cal = Calendar.getInstance();
        cal.setTime(flight.getArrivalDate());
        final String arrivalDate = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.DAY_OF_MONTH);
        dto.setArrivalDate(arrivalDate);
        final String arrivalTime = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        dto.setArrivalTime(arrivalTime);

        cal.setTime(flight.getDepartureDate());
        final String departureDate = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.DAY_OF_MONTH);
        dto.setDepartureDate(departureDate);
        final String departureTime = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        dto.setDepartureTime(departureTime);

        return dto;
    }
}
