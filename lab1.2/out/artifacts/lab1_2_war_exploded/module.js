'use strict';
angular.module("airport", ['ngRoute'])
    .service("AuthService", function ($http) {
        var admin = 'ADMIN';

        var loggedUser;
        var isAdmin;

        this.login = function (user) {
            loggedUser = user;
            isAdmin = user.type === admin;
        };

        this.isLoggedIn = function () {
            return !!loggedUser;
        };

        this.logout = function () {
            loggedUser = undefined;
        };

        this.isAdmin = function () {
            if (isAdmin === undefined) {
                $http.get('login').then(
                    function success(response) {
                        isAdmin = response.data === "true";
                        return isAdmin;
                    },
                    function error() {
                        isAdmin = undefined;
                    }
                )
            } else {
                return isAdmin;
            }
        }
    })

    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'login/login.html',
                controller: 'loginController'
            })
            .when('/flights', {
                templateUrl: 'flight/flight-list.html',
                controller: 'flightListController'
            })
            .when('/flights/new', {
                templateUrl: 'flight/flight-details.html',
                controller: 'flightDetailsController'
            })
            .when('/flights/:id', {
                templateUrl: 'flight/flight-details.html',
                controller: 'flightDetailsController'
            })
    })

    .controller("loginController", function ($scope, $http, $location, AuthService) {
        if (AuthService.isLoggedIn()) {
            $location.path('/flights');
        }

        $scope.login = function () {
            $http.post('login', $scope.user)
                .then(function (response) {
                    AuthService.login(response.data);
                    $location.path('/flights');
                });
        }
    })

    .controller("flightListController", function ($scope, AuthService, $http, $location) {
        $scope.flights = {};
        $scope.cities = {};

        $http.get('flights').then(
            function success(response) {
                $scope.flights = response.data;
            },
            function error(response) {
                if (response.status == 401) {
                    $location.path('/flights');
                } else {
                    $location.path('/');
                }
            }
        );

        $scope.new = function () {
            $location.path('/flights/new');
        };

        $scope.edit = function (flight) {
            $location.path('/flights/' + flight.id);
        };

        $scope.delete = function (flight) {
            $http.delete('flights?id=' + flight.id);
            $location.path('/flights');
        };

        $scope.isAdmin = function () {
            return AuthService.isAdmin();
        };

        $scope.logout = function () {
            $http.delete('/login');
            AuthService.logout();
            $location.path('/');
        }
    })

    .controller("flightDetailsController", function ($scope, AuthService, $http, $location, $routeParams) {
        if ($routeParams.id) {
            $http.get('/flights?id=' + $routeParams.id).then(
                function success(response) {
                    $scope.editDetail = response.data;
                    $scope.editDetail.departureCity = JSON.stringify($scope.editDetail.departureCity);
                    $scope.editDetail.arrivalCity = JSON.stringify($scope.editDetail.arrivalCity);
                },
                function error(response) {
                    if (response.status == 401) {
                        $location.path('/flights');
                    } else {
                        $location.path('/');
                    }
                }
            );
        }

        $http.get('cities').then(
            function success(response) {
                $scope.cities = response.data;
            },
            function error(response) {
                if (response.status == 401) {
                    $location.path('/flights');
                } else {
                    $location.path('/');
                }
            }
        );

        $scope.save = function () {
            $scope.editDetail.departureCity = JSON.parse($scope.editDetail.departureCity);
            $scope.editDetail.arrivalCity = JSON.parse($scope.editDetail.arrivalCity);

            $http.post('flights', $scope.editDetail).then(function () {
                $location.path('/flights');
            });
        };

        $scope.cancel = function () {
            $scope.editDetail = undefined;
            $location.path('/flights');
        }
    });